const express = require('express')
const app = express()

//Importo la conexion con mongoDB
const miconexion = require('./conexion')

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
//Uso bodyParser

const rutas = require('./routers/routers')
app.use('/api', rutas)

//Peticion get por defecto
app.get('/', (req, res) => {
    res.end("Servidor Backend OK!")
})

//Servidor 
app.listen(3000, function(){
    console.log("Servidor OK en puerto 3000 - http://localhost:3000")
})