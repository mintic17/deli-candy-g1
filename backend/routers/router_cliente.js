const express = require('express');
const router = express.Router();
const controladorCliente = require('../controllers/controller_cliente');

router.get("/", controladorCliente)
router.get("/:id", controladorCliente)
router.post("/crear", controladorCliente)
router.put("/editar/:id", controladorCliente)
router.delete("/borrar/:id", controladorCliente)

module.exports = router;