const express = require('express');
const router = express.Router();
const controladorCategorias = require('../controllers/controller_categoria');

router.get("/", controladorCategorias)
router.get("/:id", controladorCategorias)
router.post("/crear", controladorCategorias)
router.put("/editar/:id", controladorCategorias)
router.delete("/borrar/:id", controladorCategorias)

module.exports = router;