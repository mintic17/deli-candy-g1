const express = require('express');
const router = express.Router();
const controladorProducto = require('./router_producto');
const controladorCliente= require('./router_cliente');
const controladorCategoria= require('./router_categoria')

router.use("/productos", controladorProducto);
router.use("/clientes", controladorCliente);
router.use("/categorias", controladorCategoria);


module.exports = router;