const express = require('express');
const router = express.Router();
const controladorProductos = require('../controllers/controller_producto');

router.get("/", controladorProductos)
router.get("/:id", controladorProductos)
router.post("/crear", controladorProductos)
router.put("/editar/:id", controladorProductos)

module.exports = router;