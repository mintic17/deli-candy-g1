//Validation
const Joi = require('@hapi/joi')

const validacionCliente = datos => {
    const schema = Joi.object({
        documento: Joi.string().required(),
        nombre: Joi.string().required(),
        telefono: Joi.string().required(),
        direccion: Joi.string().required(),
        correo: Joi.string().required().email()

    })
    return schema.validate(datos);
};

module.exports.validacionCliente = validacionCliente;
