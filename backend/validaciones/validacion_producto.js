//Validation
const Joi = require('@hapi/joi')

const validacionProducto = datos => {
    const schema = Joi.object({
        nombre: Joi.string().required(),
        idCategoria: Joi.string().required(),
        proveedor: Joi.string(),
        stock: Joi.number().required(),
        precioVenta: Joi.number().required(),
        precioCompra: Joi.number().required(),
        activo: Joi.boolean().required()
    })
    return schema.validate(datos);
};

module.exports.validacionProducto = validacionProducto;