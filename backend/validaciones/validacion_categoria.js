const Joi = require('@hapi/joi')

const validacionCategoria = datos => {
    const schema = Joi.object({
        nombre: Joi.string().required(),
        activo: Joi.boolean().required()
    })
    return schema.validate(datos);
};

module.exports.validacionCategoria = validacionCategoria;