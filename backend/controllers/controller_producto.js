const express = require('express');
const router = express.Router();
const {validacionProducto} = require('../validaciones/validacion_producto')
const modeloProducto = require('../models/model_producto')
module.exports = router

router.get('/', (req, res) => {
    modeloProducto.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.post('/crear', (req, res) => {

    const { error } = validacionProducto(req.body)
    if (error) return res.status(400).send({ message: error.details[0].message})
    
    const nuevoProducto = new modeloProducto({
        nombre : req.body.nombre,
        idCategoria : req.body.idCategoria,
        proveedor : req.body.proveedor,
        stock : req.body.stock,
        precioVenta : req.body.precioVenta,
        precioCompra : req.body.precioCompra,
        activo : req.body.activo
    })

    nuevoProducto.save(function(err)
    {
        if(!err)
        {
            res.send('El producto fue agregado exitosamente!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//buscar producto por id
router.get('/:id', (req, res) => {
    modeloProducto.findById(req.params.id, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.put('/editar/:id', (req, res) => {
    modeloProducto.findOne({id : req.params.id})
    .then((producto) => {
        console.log(producto)
        if(producto) {
            producto.nombre = req.body.nombre ? req.body.nombre : producto.nombre,
            producto.idCategoria = req.body.idCategoria ? req.body.idCategoria : producto.idCategoria,
            producto.proveedor = req.body.proveedor ? req.body.proveedor : producto.proveedor,
            producto.stock = req.body.stock ? req.body.stock : producto.stock,
            producto.precioVenta = req.body.precioVenta ? req.body.precioVenta : producto.precioVenta,
            producto.precioCompra = req.body.precioCompra ? req.body.precioCompra : producto.precioCompra,
            producto.activo = req.body.activo 
            producto
              .save()
              .then(() => {
                res.send("El producto se actualizó exitosamente")
              });
    
        }
        else{
            res.send("El producto no existe, verificar el identificador")
        }

      });
})

module.exports = router