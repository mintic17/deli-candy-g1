const express = require('express');
const router = express.Router();
const {validacionCategoria} = require('../validaciones/validacion_categoria')
const modeloCategoria = require('../models/model_categoria')
module.exports = router

router.get('/', (req, res) => {
    modeloCategoria.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.post('/crear', (req, res) => {

    const { error } = validacionCategoria(req.body)
    if (error) return res.status(400).send({ message: error.details[0].message})
    
    const nuevoCategoria = new modeloCategoria({
        nombre : req.body.nombre,
        activo : req.body.activo
    })

    nuevoCategoria.save(function(err)
    {
        if(!err)
        {
            res.send('La categoria fue agregado exitosamente!')
        }
        else
        {
            res.send(err.stack)
        }
    })
})

//buscar categoria por id
router.get('/:id', (req, res) => {
    modeloCategoria.findById(req.params.id, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.put('/editar/:id', (req, res) => {
    modeloCategoria.findOne({id : req.params.id})
    .then((categoria) => {
        console.log(categoria)
        if(categoria) {
            categoria.nombre = req.body.nombre ? req.body.nombre : categoria.nombre,
            categoria.activo = req.body.activo
            categoria
              .save()
              .then(() => {
                res.send("La categoria se actualizó exitosamente")
              });
    
        }
        else{
            res.send("La categoria no existe, verificar el identificador")
        }

      });
})

router.delete('/borrar/:id', (req, res) => {

    modeloCategoria.findOne
    ({
        id: req.params.id
    })
    .then((categoria) => {
        if(categoria) 
        {
            categoria
              .delete()
              .then(() => {
                res.send("La categoria se eliminó exitosamente")
              });
    
        }
        else
        {
            res.send("La categoria no existe, verificar documento")
        }

    });
})


module.exports = router