const express = require('express');
const router = express.Router();
const { validacionCliente } = require('../validaciones/validacion_cliente')
const modeloCliente = require('../models/model_cliente')
module.exports = router

router.get('/', (req, res) => {
    modeloCliente.find({}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

router.post('/crear', (req, res) => {

    const { error } = validacionCliente(req.body)
    if (error) return res.status(400).send({ message: error.details[0].message })

    modeloCliente.exists({documento: req.body.documento})
    .then((existe) => {
        if (!existe){
            const nuevoCliente = new modeloCliente({
                documento : req.body.documento,
                nombre : req.body.nombre, 
                telefono : req.body.telefono, 
                direccion : req.body.direccion, 
                correo : req.body.correo
            })
        
            nuevoCliente.save(function(err)
            {
                if(!err)
                {
                    res.send('El cliente fue agregado exitosamente!')
                }
                else
                {
                    res.send(err.stack)
                }
            })
        }
        else
        {
            res.send("El cliente ya existe")
        }
    }) 
   
})

//buscar por id
router.get('/:documento', (req, res) => {
    modeloCliente.findOne({documento: req.params.documento}, function(docs, err)
    {
        if(!err)
        {
            res.send(docs)
        }
        else
        {
            res.send(err)
        }
    })
})

//actualizar
router.put('/editar/:documento', (req, res) => {

    modeloCliente.findOne
    ({
        documento: req.params.documento
    })

    .then((cliente) => {
        if(cliente) 
        {
            cliente.nombre = req.body.nombre ? req.body.nombre : cliente.nombre, 
            cliente.telefono = req.body.telefono ? req.body.telefono : cliente.telefono, 
            cliente.direccion = req.body.direccion ? req.body.direccion : cliente.direccion,
            cliente.correo = req.body.correo ? req.body.correo : cliente.correo,
            cliente
              .save()
              .then(() => {
                res.send("Cliente actualizado exitosamente")
              });
    
        }
        else
        {
            res.send("Cliente no existe, verificar documento")
        }

    });
})

router.delete('/borrar/:documento', (req, res) => {

    modeloCliente.findOne
    ({
        documento: req.params.documento
    })
    .then((cliente) => {
        if(cliente) 
        {
            cliente
              .delete()
              .then(() => {
                res.send("El cliente se eliminó exitosamente")
              });
    
        }
        else
        {
            res.send("Cliente no existe, verificar documento")
        }

    });
})

module.exports = router