const mongoose = require('mongoose')
const miesquema = mongoose.Schema

const producto = new miesquema({
    nombre : {
        type: String,
        required: true
    }, 

    idCategoria : {
        type: String,
        required: true
    }, 

    proveedor : {
        type: String,
        required: false
    }, 

    stock : {
        type: Number,
        required: true
    }, 

    precioVenta : {
        type: Number,
        required: true
    }, 

    precioCompra : {
        type: Number,
        required: true
    }, 
    
    activo : {
        type: Boolean,
        required: true
    }
}, { versionKey: false });

const modeloProducto = mongoose.model('productos', producto)
module.exports = modeloProducto;