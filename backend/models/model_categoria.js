const mongoose = require('mongoose')
const miesquema = mongoose.Schema

const categoria = new miesquema({
    nombre : {
        type: String,
        required: true
    }, 

    activo : {
        type: Boolean,
        required: true
    }

}, { versionKey: false });

const modeloCategoria = mongoose.model('categorias', categoria)
module.exports = modeloCategoria;