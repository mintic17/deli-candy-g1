const mongoose = require('mongoose')
const miesquema = mongoose.Schema

const cliente = new miesquema({
    documento : {
        type: String,
        required: true
    },
    nombre : {
        type: String,
        required: true
    },  
    telefono : {
        type: String,
        required: true
    },  
    direccion : {
        type: String,
        required: true
    },  
    correo : {
        type: String,
        required: true
    }  
}, { versionKey: false });
const modeloCliente = mongoose.model('clientes', cliente)

module.exports = modeloCliente;